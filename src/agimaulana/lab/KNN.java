package agimaulana.lab;


import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class KNN {
    private final String DATA_SET_FILE_NAME = "asset/Dataset-Tugas-3-AI-1718.xls";
    private final String DATA_TRAIN_SHEET = "DataTrain";
    private final String DATA_TEST_SHEET = "DataTest";
    private final int FOLD = 4;
    private int foldRows;
    private final int K = 255;
    Workbook workbook;
    private Sheet dataTrainSheet;
    private Sheet dataTestSheet;

    public KNN(){
        try {
             workbook = Workbook.getWorkbook(new File(DATA_SET_FILE_NAME));
             dataTrainSheet = workbook.getSheet(DATA_TRAIN_SHEET);
             dataTestSheet = workbook.getSheet(DATA_TEST_SHEET);
             int totalRow = dataTrainSheet.getRows()-1;
             foldRows = totalRow / FOLD;
//            System.out.println(Arrays.asList(dataTrainSheet.getRow(4000)).get(0).getContents());
//            System.out.println(dataTrainSheet.getRows());
        } catch (IOException | BiffException e) {
            e.printStackTrace();
        }
    }

    public void exec(){
        exec(1);
    }

    private void exec(int fold){
        new Thread(() -> train(fold)).start();
    }

    private void train(int foldIteration){
        long start = System.nanoTime();
        List<List<Cell>> trainCells = new ArrayList<>();
        List<List<Cell>> testCells = new ArrayList<>();

        // get test cells
//        if(foldIteration == 0)
        System.out.println("Test " + foldIteration + " processing. Please wait....");
        int maxTestRange = foldIteration * foldRows;
        int minTestRange = maxTestRange - foldRows + 1;
        System.out.println("Test Range : " + minTestRange + " - " + maxTestRange);

        for (int i = 1 ; i < dataTrainSheet.getRows()-1; i++){
            if(i >= minTestRange && i <= maxTestRange)
                testCells.add(Arrays.asList(dataTrainSheet.getRow(i)));
            else
                trainCells.add(Arrays.asList(dataTrainSheet.getRow(i)));
        }

        News tesNews;
        List<Distance> distances = new ArrayList<>();
        String fileName = "Test " + foldIteration + ".xls";
        String percentageFileName = "Percentage.xls";
        for (List<Cell> testCell: testCells) {
             tesNews = NewsFactory.fromCell(testCell);
             distances.clear();
            for (List<Cell> trainCell : trainCells){
                News trainNews = NewsFactory.fromCell(trainCell);
                distances.add(new Distance(trainNews).computeDistance(tesNews));
            }
            distances.sort(Comparator.comparing(Distance::getDistance));
            distances.subList(K, distances.size()).clear();
            Spreadsheet.name(fileName)
                    .printTest(tesNews, mostCommonHoax(distances));
        }
        System.out.println("Test done.");
        System.out.println("Calculating percentage to " + percentageFileName);
        Spreadsheet.name(percentageFileName)
                .printPercentage(fileName);

        System.out.println("Test " + foldIteration + " process finish.");
        long estimatedTime = System.nanoTime() - start;
        System.out.println("Elapsed Time :  " + TimeUnit.NANOSECONDS.toSeconds(estimatedTime) + " second(s)\n");

        if(foldIteration < FOLD)
            exec(foldIteration+1);
        else if(foldIteration == FOLD)
            validateTestData();
    }

    public void validateTestData(){
        long start = System.nanoTime();
        System.out.println("Data Test processing. Please wait....");
        News news = null;
        List<Cell> trainCells;
        List<Distance> distances = new ArrayList<>();
        String filename = "DataTestResult.xls";
        for (int i = 1; i < dataTestSheet.getRows()-1; i++){
            distances.clear();
            news = NewsFactory.fromCell(Arrays.asList(dataTestSheet.getRow(i)));
            for (int j = 1; j < dataTrainSheet.getRows()-1; j++){
                trainCells = Arrays.asList(dataTrainSheet.getRow(j));
                distances.add(new Distance(NewsFactory.fromCell(trainCells))
                    .computeDistance(news));
            }
            distances.sort(Comparator.comparing(Distance::getDistance));
            distances.subList(K, distances.size()).clear();
            Spreadsheet.name(filename)
                    .printValidation(news, mostCommonHoax(distances));
        }

        System.out.println("Data Test processing finish.");
        long estimatedTime = System.nanoTime() - start;
        System.out.println("Elapsed Time :  " + TimeUnit.NANOSECONDS.toSeconds(estimatedTime) + " second(s)\n");
    }

    private int mostCommonHoax(List<Distance> distances){
        int notHoax = 0;
        int isHoax = 0;

        for (int i = 0; i < K; i++){
            if(distances.get(i).getTrain().getHoax() == 0)
                notHoax++;
            else
                isHoax++;
        }
        return notHoax > isHoax ? 0 : 1;
    }

}
