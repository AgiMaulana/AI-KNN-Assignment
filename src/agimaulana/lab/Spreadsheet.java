package agimaulana.lab;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Spreadsheet {
    private static final int INDEX_COLUMN_ID = 0;
    private static final int INDEX_COLUMN_LIKE = 1;
    private static final int INDEX_COLUMN_PROVOCATION = 2;
    private static final int INDEX_COLUMN_COMMENT = 3;
    private static final int INDEX_COLUMN_EMOTION = 4;
    private static final int INDEX_COLUMN_HOAX = 5;
    private static final int INDEX_COLUMN_HOAX_TEST = 6;
    private static final int INDEX_COLUMN_PERCENTAGE_VALUE = 1;

    private final String COLUMN_NEWS_ID = "News";
    private final String COLUMN_NEWS_LIKE = "Like";
    private final String COLUMN_NEWS_PROVOCATION = "Provocation";
    private final String COLUMN_NEWS_COMMENT = "Comment";
    private final String COLUMN_NEWS_EMOTION = "Emotion";
    private final String COLUMN_NEWS_HOAX = "Hoax";
    private final String COLUMN_NEWS_HOAX_TEST = "Test Hoax";
    private final String COLUMN_PERCENTAGE_TEST_NAME = "Test";
    private final String COLUMN_PERCENTAGE_VALUE = "Percentage";

    private String fileName;
    private Workbook workbook;
    private WritableWorkbook writableWorkbook;

    private Spreadsheet(String fileName){
        this.fileName = fileName;
        File file = new File("output", fileName);
        try {
            File dir = new File("output");
            if(!dir.exists())
                dir.mkdir();

            if(file.exists()) {
                workbook = Workbook.getWorkbook(file);
                writableWorkbook = Workbook.createWorkbook(file, workbook);
            }else {
                writableWorkbook = Workbook.createWorkbook(file);
            }
        } catch (IOException | BiffException e) {
            e.printStackTrace();
        }
    }

    public static Spreadsheet name(String name){
        return new Spreadsheet(name);
    }

    public synchronized void printTest(News news, int hoax){
        WritableSheet sheet = writableWorkbook.getSheet(fileName);
        if(sheet == null)
            sheet = writableWorkbook.createSheet(fileName, 0);
        try {
            sheet.addCell(new Label(0, 0, COLUMN_NEWS_ID));
            sheet.addCell(new Label(1, 0, COLUMN_NEWS_LIKE));
            sheet.addCell(new Label(2, 0, COLUMN_NEWS_PROVOCATION));
            sheet.addCell(new Label(3, 0, COLUMN_NEWS_COMMENT));
            sheet.addCell(new Label(4, 0, COLUMN_NEWS_EMOTION));
            sheet.addCell(new Label(5, 0, COLUMN_NEWS_HOAX));
            sheet.addCell(new Label(6, 0, COLUMN_NEWS_HOAX_TEST));

            int row = sheet.getRows();
            sheet.addCell(new Label(0, row, news.getId()));
            sheet.addCell(new Label(1, row, String.valueOf(news.getLike())));
            sheet.addCell(new Label(2, row, String.valueOf(news.getProvocation())));
            sheet.addCell(new Label(3, row, String.valueOf(news.getComment())));
            sheet.addCell(new Label(4, row, String.valueOf(news.getEmotion())));
            sheet.addCell(new Label(5, row, String.valueOf(news.getHoax())));
            sheet.addCell(new Label(6, row, String.valueOf(hoax)));
            writableWorkbook.write();
            writableWorkbook.close();
        } catch (WriteException | IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void printDistance(List<Distance> distances){
        WritableSheet sheet = writableWorkbook.getSheet(fileName);
        if(sheet == null)
            sheet = writableWorkbook.createSheet(fileName, 0);
        try {
            sheet.addCell(new Label(0, 0, COLUMN_NEWS_ID));
            sheet.addCell(new Label(1, 0, COLUMN_NEWS_LIKE));
            sheet.addCell(new Label(2, 0, COLUMN_NEWS_PROVOCATION));
            sheet.addCell(new Label(3, 0, COLUMN_NEWS_COMMENT));
            sheet.addCell(new Label(4, 0, COLUMN_NEWS_EMOTION));
            sheet.addCell(new Label(5, 0, COLUMN_NEWS_HOAX));
            sheet.addCell(new Label(6, 0, "Distance"));

            for (Distance d : distances){
                int row = sheet.getRows();
                sheet.addCell(new Label(0, row, d.getTrain().getId()));
                sheet.addCell(new Label(1, row, String.valueOf(d.getTrain().getLike())));
                sheet.addCell(new Label(2, row, String.valueOf(d.getTrain().getProvocation())));
                sheet.addCell(new Label(3, row, String.valueOf(d.getTrain().getComment())));
                sheet.addCell(new Label(4, row, String.valueOf(d.getTrain().getEmotion())));
                sheet.addCell(new Label(5, row, String.valueOf(d.getTrain().getHoax())));
                sheet.addCell(new Label(6, row, String.valueOf(d.getDistance())));
            }

            writableWorkbook.write();
            writableWorkbook.close();
        } catch (WriteException | IOException e) {
            e.printStackTrace();
        }
    }

    public void printPercentage(String testSource){
        try {
            File file = new File("output", testSource);
            Workbook source = Workbook.getWorkbook(file);
            Sheet sourceSheet = source.getSheet(testSource);
            double avg = 0;
            for (int i = 1; i < sourceSheet.getRows(); i++){
                int hoax = Integer.parseInt(sourceSheet.getRow(i)[INDEX_COLUMN_HOAX].getContents());
                int hoaxTest = Integer.parseInt(sourceSheet.getRow(i)[INDEX_COLUMN_HOAX_TEST].getContents());
                if(hoax == hoaxTest)
                    avg+=1;
            }

            avg = avg / (sourceSheet.getRows()-1) * 100;

            WritableSheet sheet = writableWorkbook.getSheet(fileName);
            if(sheet == null)
                sheet = writableWorkbook.createSheet(fileName, 0);
            sheet.addCell(new Label(0, 0, COLUMN_PERCENTAGE_TEST_NAME));
            sheet.addCell(new Label(1, 0, COLUMN_PERCENTAGE_VALUE));


            int row = sheet.getRows();
            sheet.addCell(new Label(0, row, testSource));
            sheet.addCell(new Label(1, row, String.valueOf(avg)));

            writableWorkbook.write();
            writableWorkbook.close();
        } catch (IOException | BiffException | WriteException e) {
            e.printStackTrace();
        }
        printAverage();
    }

    private void printAverage(){
        try {
            File file = new File("output",fileName);
            if(file.exists()) {
                workbook = Workbook.getWorkbook(file);
                writableWorkbook = Workbook.createWorkbook(file, workbook);
            }else {
                writableWorkbook = Workbook.createWorkbook(file);
            }
            double avg = 0;
            Sheet sheet = workbook.getSheet(fileName);
            int row = 0;
            for (int i = 1; i < sheet.getRows(); i++){
//                System.out.println(sheet.getRow(i)[0].getContents());
                avg += Double.parseDouble(sheet.getRow(i)[INDEX_COLUMN_PERCENTAGE_VALUE].getContents());
                row += 1;
            }

            if(avg > 0)
                avg = avg / row;

            int column = 3;
            WritableSheet writableSheet = writableWorkbook.getSheet(fileName);
            writableSheet.addCell(new Label(column, 0, "AVERAGE"));
            writableSheet.addCell(new Label(column+1, 0, String.valueOf(avg)));
            writableWorkbook.write();
            writableWorkbook.close();
        } catch (WriteException | IOException | BiffException e) {
            e.printStackTrace();
        }
    }

    public synchronized void printValidation(News news, int hoax){
        WritableSheet sheet = writableWorkbook.getSheet(fileName);
        if(sheet == null)
            sheet = writableWorkbook.createSheet(fileName, 0);
        try {
            sheet.addCell(new Label(0, 0, COLUMN_NEWS_ID));
            sheet.addCell(new Label(1, 0, COLUMN_NEWS_LIKE));
            sheet.addCell(new Label(2, 0, COLUMN_NEWS_PROVOCATION));
            sheet.addCell(new Label(3, 0, COLUMN_NEWS_COMMENT));
            sheet.addCell(new Label(4, 0, COLUMN_NEWS_EMOTION));
            sheet.addCell(new Label(5, 0, COLUMN_NEWS_HOAX));

            int row = sheet.getRows();
            sheet.addCell(new Label(0, row, news.getId()));
            sheet.addCell(new Label(1, row, String.valueOf(news.getLike())));
            sheet.addCell(new Label(2, row, String.valueOf(news.getProvocation())));
            sheet.addCell(new Label(3, row, String.valueOf(news.getComment())));
            sheet.addCell(new Label(4, row, String.valueOf(news.getEmotion())));
            sheet.addCell(new Label(5, row, String.valueOf(hoax)));
            writableWorkbook.write();
            writableWorkbook.close();
        } catch (WriteException | IOException e) {
            e.printStackTrace();
        }
    }
}
