package agimaulana.lab;

import jxl.Cell;

import java.util.List;

public class NewsFactory {
    private static final int COLUMN_ID = 0;
    private static final int COLUMN_LIKE = 1;
    private static final int COLUMN_PROVOCATION = 2;
    private static final int COLUMN_COMMENT = 3;
    private static final int COLUMN_EMOTION = 4;
    private static final int COLUMN_HOAX = 5;

    public static News fromCell(List<Cell> cells){
        int hoax = 0;
        try {
            hoax = Integer.parseInt(cells.get(COLUMN_HOAX).getContents());
        }catch (NumberFormatException e){
            //
        }
        return new News(
                cells.get(COLUMN_ID).getContents(),
                Integer.parseInt(cells.get(COLUMN_LIKE).getContents()),
                Integer.parseInt(cells.get(COLUMN_PROVOCATION).getContents()),
                Integer.parseInt(cells.get(COLUMN_COMMENT).getContents()),
                Integer.parseInt(cells.get(COLUMN_EMOTION).getContents()),
                hoax
        );
    }
}
