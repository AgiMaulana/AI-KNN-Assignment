package agimaulana.lab;

public class News {
    private String id;
    private int like;
    private int provocation;
    private int comment;
    private int emotion;
    private int hoax;

    public News(String id, int like, int provocation, int comment, int emotion, int hoax) {
        this.id = id;
        this.like = like;
        this.provocation = provocation;
        this.comment = comment;
        this.emotion = emotion;
        this.hoax = hoax;
    }

    public String getId() {
        return id;
    }

    public int getLike() {
        return like;
    }

    public int getProvocation() {
        return provocation;
    }

    public int getComment() {
        return comment;
    }

    public int getEmotion() {
        return emotion;
    }

    public int getHoax() {
        return hoax;
    }
}
