package agimaulana.lab;

public class Distance {
    private News train;
    private double distance;

    public Distance(News train) {
        this.train = train;
    }

    public Distance computeDistance(News test){
        double like = Math.pow(test.getLike() - train.getLike(), 2);
        double provocation = Math.pow(test.getProvocation() - train.getProvocation(), 2);
        double comment = Math.pow(test.getComment() - train.getComment(), 2);
        double emotion = Math.pow(test.getEmotion() - train.getEmotion(), 2);
        this.distance = Math.sqrt(like + provocation + comment + emotion);
        return this;
    }

    public News getTrain() {
        return train;
    }

    public double getDistance() {
        return distance;
    }
}
